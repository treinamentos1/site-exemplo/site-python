FROM python:3.9.6-alpine

ADD . /opt/flask
RUN pip3 install flask 

CMD ["python","/opt/flask/app.py"]