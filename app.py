from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route("/")
def home():
    host = os.uname()[1]
    print(host)
    return render_template('home.html', titulo = 'Bem-vindo', hostname = host )

if __name__ == "__main__":
    app.run(host="0.0.0.0")